#!/usr/bin/perl -w

# written by ashley rodrigues UNSW
# z5015076@student.unsw.edu.au

# Input Parameters: array, file name
# Function: Writes the contents of the array to the file
sub writeToFile {
	foreach my $line (@{ $_[0] }) {
        print "$line";
    }
}

# Input: module name, output file array
# Function: Adds import statement to the top of the file if
# the module provided isnt present
sub addMod {
    if (not defined $importLib{$_[0]}) {
        my $declaration = shift (@{ $_[1 ] });
		unshift (@{ $_[1] }, "import $_[0]\n");
		unshift (@{ $_[1] }, $declaration);
        $importLib{$_[0]} = 1;
    } 
}

# Handles statments with an echo
# Including redirecting outout to files
sub echoTo {
    $_[0] =~ s/^ +//;
    my $retStr .= " " x (4* ${ $_[3] }); 
    if ($_[0] =~ /^echo -n "(.*)"/) {
        addMod("sys", $_[1]);
        $retStr .= "sys.stdout.write($1)\n";
    } elsif ($_[0] =~ /^echo +\$(\w+) +\>\>\$(\w+)/) {
        $retStr .= "with open($2, 'a') as f: print >>f, $1\n";
    } elsif ($_[0] =~ /^echo (\"|\')(.*)(\"|\')$/) {
        my $container = $1; # Type of quote contains string
        my $contString = $2; # Contained string
        if ($1 eq "\"") { 
            # Handles double quotes
            $contString =~ s/^\"*|[^\\]\"*$//;
            $contString =~ s/[^\\]\"//g;
            $contString =~ s/\$(\w+)/\" \+ $1 \+ \"/g;        
            #print "$contString\n";            
        }
        $retStr .= "print $container$contString$container\n";

    } elsif ($_[0] =~ /echo [^ ]+/) {
        #Handles no quotes
		$_[0] =~ s/^echo //;
        my $sepStr = toSepStr($_[0], $_[1], 1);
        $retStr .= "print".$sepStr."\n";
    } else {
        $retStr = "";
    }
    push (@{ $_[1] }, $retStr); 
}

# ls Function handler, uses default subprc function if string is
# of default format or does not handle string if lien is unknown


# Handles system/unix commands using the sub process call
# Loops through words in system command and attaches in appropriate
# place corresponding to regex match
sub subPrcCmd {
    my $sysCmd = $1 if ($_[0] =~ /^([a-z]+)/);
    $_[0] =~ s/$sysCmd//;
    my $retStr .= " " x (4* ${ $_[3] });
	addMod("subprocess", $_[1]);
    $retStr .= "subprocess.call([";
    $retStr .= $sysCmd;
    my @words = $_[0] =~ /[^ ]+/g;
    my @var = ();
    foreach $word (@words) {
        if ($word =~ /^\"\$\@\"$/) {
            push(@var, "str(".toVar($word, $_[1]).")");
        } elsif (toVar($word, $_[1])) {
            $retStr .= ", '$word'";
        }
    }
    $retStr .= "]";
    foreach my $arg (@var) {
        $retStr .= " + $arg";
    }
    $retStr .= ")\n";
    push(  @{ $_[1] }, $retStr);
}

# Handles cd system command
sub cdTo {
    $_[0] =~ s/cd //;
    my $retStr = "";
    $retStr .= " " x (4* ${ $_[3] });
    $retStr .= "os.chdir('$_[0]')";
    addMod("os", $_[1]);
    push (@{ $_[1] }, $retStr);    
}

# Converts system exit to python system exit
sub exitTo {
    my $retStr = "";
    $retStr .= " " x (4* ${ $_[3] });
    addMod("sys", $_[1]);
    my $exStatus = $1 if ($_[0] =~ /(\d+$)/);
    $retStr .= "sys.exit($exStatus)\n";
    push (@{ $_[1] }, $retStr);
}

# Converts read command
sub readTo {
    addMod("sys", $_[1]);
    my $retStr = "";
    $retStr .= " " x (4* ${ $_[3] });
    my @words = split(' ', $_[0]);
    $retStr .= "$words[1] = sys.stdin.readline().rstrip()\n";
    push(@{ $_[1] }, $retStr);    
}

# Handles for loops
# Case 1: # elements in string
# Case 2: files in directory
sub forTo {
    my $retStr = "";
    $retStr .= " " x (4* ${ $_[3] });
    $_[0] =~ /for +(\w+) +in +(.+)/; 
    my $var1 = $1;
    my $var = $2;
    $var =~ s/:$//g;
    $retStr .= "for $var1 in";
    if ($var =~ /^[\w ]+/g) {
        $retStr .= toSepStr($var, $_[1]).":\n";
    } elsif ($var =~ /(.*\.\w+)/) {
        $retStr .= " sorted(glob.glob(\"$1\")):\n"; 
    }
    push (@{ $_[1] }, $retStr);
}

# Handles while loops
sub whileTo {
    my $retStr = "";
    $retStr .= " " x (4* ${ $_[3] });
    $_[0] =~ s/while //;
    $_[0] =~ s/test //;
    $_[0] =~ s/:$//;
    my $convStr =  condToPyCond($_[0], $_[1]);
    my $var = $1;
    $retStr .= "while ".$convStr.":\n";
    push (@{ $_[1] }, $retStr);
}


# Handles if and elsif statements since they're similar
# Usses different indentation rules
sub ifTo {
    my $retStr;
    my $dup = $_[0];
    $dup =~ s/^if\s+|elsif\s+//;
    $dup =~ s/:$//;
    $dup =~ s/test\s+//;
    $dup =~ s/^\[\s+//;
    $dup =~ s/\s+\]$//;
    
    my $convStr =  condToPyCond($dup, $_[1]).":\n"; 
    if ($_[0] =~ /^if /) {
        $retStr .= " " x (4* ${ $_[3] });
        $retStr .= "if $convStr";
    } else {
		decrIndent($_[0], $_[1], $_[2], $_[3]);
        $retStr .= " " x (4* (${ $_[3] }));
        $retStr .= "elif $convStr"; 
    }
    push (@{ $_[1] }, $retStr);   
}

# Handles the specical else case
# Different to if/elsif since no condition is required
sub elseTo {
    my $retStr .= " " x (4* (${ $_[3] } -1));
    $retStr .= "else:\n";
    push (@{ $_[1] }, $retStr);   
}


# Handes case statemnts, fist pass stores main case in global variable
# and sets flag
sub caseTo {
    if ($_[0] =~ /^case/) { 
        my $mainCase = $_[0];
        $mainCase =~ s/^case\s+//;
        $mainCase =~ s/\s+in.*//;
        $typeFlag = "case"; 
        $globVar = toVar($mainCase, $_[1]) ? toVar(toVar($mainCase, $_[1])) : "\'$mainCase\'";
        incrIndent(0, 0, 0, $_[3]);
        return;
    } else {
        if ($_[0] =~ /(.*)\)\s*/) {
            my $retStr .= " " x (4* (${ $_[3] } -1));
            my $cases = $1;
            $cases =~ s/,//g;
            $cases = toSepStr($cases,$_[1]);
            $cases =~ s/,//g;
            $retStr .= "if ".$globVar." in".$cases.":\n";
            push (@{$_[1]}, $retStr);
        }
    }
}

# Handles custom expr statements, these are different to standard expr statements
sub exprTo {
    $_[0] =~ s/[^ ]+ //;
    $_[0] =~ s/\`//;    
    my @words = $_[0] =~ /[^ ]+/g; 
    my $retStr = "";
    foreach $word (@words) {
        if (toVar($word, $_[1])) {
           $retStr.= " int(".toVar($word, $_[1]).")";
        } elsif ($word =~ /\'\*\'/) {
            $retStr .= " *";
        } else { 
             $retStr .=" $word"; 
        }
    }
    return $retStr;
}

# Increments global indent variable
sub incrIndent {
    ${ $_[3] }++;
}

# Decrements global indent variable
sub decrIndent {
    ${ $_[3] }--;
}

# Changes a bash condition to a python condition
sub condToPyCond {
    my @words = $_[0] =~ /[^ ]+/g;
    my $convStr = "";
    my @splitCond = split(/\s+(-o|-a)\s+/, $_[0]);
    if ($#splitCond > 0) {   
        foreach my $cond (@splitCond) {
            $convStr .= " ".condToPyCond($cond, $_[1]); 
        }
    } else { 
        for (my $i = 0; $i < $#words+1; $i++) {
            my $word = $words[$i];
            if (defined $sysCmd{$word}) {
                subPrcCmd($_[0], $_[1], 0, \0);
                $convStr .= "not ";
                my $temp = pop @{ $_[1] }; 
                chomp $temp;
                $convStr .= $temp;               
                last;
            } elsif ($word eq "=" ) { 
                $convStr .= " ".$opToPy{"="}." ";
            } elsif ($word =~ /^-([a-z]{2})/) {
                $convStr .= " $opToPy{$1} ";
            } elsif ($word =~ /^-([^oa])$/) {
                $convStr .= &{ $testToPy{$1} }($words[++$i], $_[1]);
            } elsif ($word =~ /^-([a-z])/) {
                $convStr .= $opToPy{$1}; 
            } elsif (toVar($word, $_[1])) {
                $convStr .= "int(".toVar($word, $_[1]).")";
            } elsif ($word =~ /^-?[0-9]\d*(\.\d+)?$/) {
                $convStr .= $word;
            } else {
                $convStr .= "'$word'";
            }
        }
    }
    $convStr =~ s/^ //;
    return "$convStr";
}

# Test whether the term is a condition conjunction
sub condTerm {
	return 1 if ($_[0] =~ /-o|a/);
	return 0;
}

# Tests whether operand is numeric
sub isNumericOp {
	return if (not $_[0]);
	return 1 if ($_[0] =~ /-eq|-ge|-le|-gt|-lt/);
	return 0;
}

# Input: dir, outarray
# Takes a directory and returns python translation
sub dIsDir {
    addMod("os", $_[1]);
    return "os.path.isdir('$_[0]')";
}

# input: file dir, output array
# Tests whether a file is readable
sub rIsReadFile {
    addMod("os", $_[1]);
    return "os.access('$_[0]', os.R_OK)";
}

# test file condition
sub eIsFileExist {
    addMod("os.path", $_[1]);
    return "os.path.isfile('$_[0]')";
}

# test file condition
sub sIsGreaterZero {
    addMod("os", $_[1]);
    return "os.path.getsize('$_[0]') > 0";
}

# Handles chmod
sub chmodTo {
    my $perm = $1, $dir = $2 if ($_[0] =~ /^chmod\s+(\d+)\s+([^ ]+$)/);
    addMod("os", $_[1]);
    push( @{ $_[1] }, "os.chmod(\"".$dir."\", ".$perm.")\n");
}

# Takes in a string and returns a separated string
# according to python syntax
sub toSepStr {
    my $str = $_[0];
    my @words = $str =~ /([^ ]+)/g;
    my $retStr = "";
    for (my $i = 0; $i < $#words; $i++) {
    $words[$i] =~ s/([^\\])\'/$1/g;
    $words[$i] =~ s/([^\\])\"/$1/g;
        if (toVar($words[$i], $_[1])) {
            my $var = toVar($words[$i], $_[1]);
			$retStr .= " $var,";
        } else {
            $retStr .= " '$words[$i]',";
        }
    }
    $words[$#words] =~ s/\'//g;
    $words[$#words] =~ s/\"//g;
    if (toVar($words[$#words], $_[1])) {
		my $var = toVar($words[$#words], $_[1]);
		$retStr .= " $var";
    } else {
         $retStr .= " '$words[$#words]'";
    }
    return $retStr;
}

sub esacTo {
    decrIndent(0, 0, 0, $_[3]);    
}

# Translates word to python variable and returns
# the translated variable if the input is a valid
# variable
sub toVar {
    my $temp = $_[0];
    if ($temp =~ /^\$(\d+)$/) {
        addMod("sys", $_[1]);
		return "sys.argv[$1]";
	} elsif ($temp =~ /^\@/) {
        addMod("sys", $_[1]);       
		return "sys.argv[1:]";
    } elsif ($temp =~ /^\$\#$/) {
        addMod("sys", $_[1]);
        return "len(sys.argv[1:])";
    } elsif ($temp =~ /^\$\*$/) {
		addMod("sys", $_[1]);
        return '" ".join(sys.argv[1:])';
    } elsif ($temp =~ /^\$(\w+)$/) {
        return "$1";
    } elsif ($temp =~ /^\"\$(\w+)\"$/) {
        return "$1";
    } elsif ($temp =~ /^`expr/) {
        return exprTo($temp);
    } elsif ($temp =~ /^(\w+)\$(\w+)/) {
        return "'$1' + $2";
    } else {
        return ""; 
    }
}

#defines a variable
# case 1: assigning variable to variable
# case 2: assigning numeric value to variable
# case 3: assigning string to variable
sub defVar {
    my $retStr .= " " x (4* (${ $_[4] } ));    
	$retStr .= "$_[0] = ";
    my $var = $_[1];
    if (toVar($var, $_[2])) {
        $retStr .= toVar($var, $_[2]);
        $varToVal{$_[0]}= defined($varToVal{toVar($var, $_[2])}) ? $varToVal{toVar($var, $_[2])} : "na";
    } elsif ($var =~ /^(-?[0-9]\d*(\.\d+)?)$/) {
        $retStr .= $1;
        $varToVal{$_[0]} = "i";
    } elsif ($var =~ /^\$\(\(\$(\w+)\s+([\+\-])\s+(\d+)\)\)/) {
        $retStr .= "int($1) $2 $3";
    } elsif ($var =~ /^`expr/) {
        $retStr .= toVar($var, $_[2]);
        $varToVal{$_[0]} = "i";
    } elsif ($var =~ /^`\w+?|^\$\((.*)\)$/) {
        $var = $1 if $1;
        $var =~ s/(^`|`$)//g;
        subPrcCmd($var, $_[2], \0, \0);
        my $temp = pop(@{ $_[2] });
        chomp $temp;
        $retStr .= $temp;
    } else {
        $retStr .= "'$var'";
        $varToVal{$_[0]} = "s";
    }
    push(@{ $_[2] }, "$retStr\n");
}

# Adds entry when module is added to file array
$importLib=();

# Map of system commands
%sysCmd = (
	grep => 1,
	egrep => 1,
	fgrep => 1,
    true => 1,
    false => 1,
    ls => 1,
    rm => 1,
    mv => 1,
);

# maps var to val
$varToVal=();

# Mapping of a command to appropriate handling function
%cmdToFunc=(
    echo => \&echoTo,
    ls => \&subPrcCmd,
    pwd => \&subPrcCmd,
    date => \&subPrcCmd,
    cd => \&cdTo,
    read => \&readTo,
    exit => \&exitTo,
    for => \&forTo,
    if => \&ifTo,
    elif => \&ifTo,
    else  => \&elseTo,
    do => \&incrIndent,
    done => \&decrIndent,
    then => \&incrIndent,
    fi => \&decrIndent,
    "`expr" => \&exprTo,
    while => \&whileTo,
    rm => \&subPrcCmd,
    chmod => \&chmodTo,
    case => \&caseTo,
    esac => \&esacTo,
    mv => \&subPrcCmd
);


# Misc file handling operations
%testToPy=(
    r => \&rIsReadFile,
    d => \&dIsDir,
    e => \&eIsFileExist,
    s => \&sIsGreaterZero
);

# Maps shell to python operands
%opToPy=(
    "eq" => "==",
    "ne" => "!=",
    "=" => "==",
    "!" => "not",
    "a" => "and",
    "o" => "or",
    "lt" => "<",
    "le" => "<=",
    "gt" => ">",
    "ge" => ">="
);

#default file location (STDIN ouput file)
#my $file = "temp.sh";

#Check whether to use file from args, or stdin
if ($#ARGV > -1) {
    $file = $ARGV[0];
} else {
    $file = "temp.sh";
    open($tempFile, '>', 'temp.sh' ) or die "Can't open $file";
    foreach my $line (<STDIN>) {
        print $tempFile "$line"; 
		#print $line;
	}
    close $tempFile;
}

# Main loop
# Begins file analysis, reads file one line at a time
@outFile = ();
my $index = 0;
# Opens file
open($tempFile, '<', $file ) or die "Can't open $file";
$typeFlag = 0; # Flag used when a large data structure is being analysed
$globVar = 0; # Global variable to store data
my $indent = 0; # Global indentation variable
# For every line in the file
foreach my $line (<$tempFile>) {
    chomp $line;
    my %quoteHash = (
        "\"" => 0,
        "\'" => 0,
        "\`" => 0
    );
    
    # Block splits conditional execution statements e.g ls ; ls
    # and stores the data into the array
    my ($prevPrevChar, $prevChar) = 0;
    my $quoteBalance = 0;
    my $newCmdFlag = 0;
    my $cmd = "";
    my @cmds = ();
    foreach my $char (split //, $line) {
        if ($char =~ /(\"|\'|\`)/) {
            $quoteHash{"$char"} = $quoteHash{"$char"} ? 0 : 1;
        }
        if ($char eq ";" and $prevChar ne "\\") {
            $newCmdFlag = ";";
        } elsif ($char eq "&" and $prevChar eq "&" and $prevPrevChar ne "\\") {
            $newCmdFlag = "&";
        } elsif ($char eq "|" and $prevChar eq "|" and $prevPrevChar ne "\\") {
            $newCmdFlag = "|";
        }
        if ($newCmdFlag) {
            my $temp = ($char eq ";") ? $cmd.$char : $cmd; 
            push(@cmds, $temp."\n");
            $cmd = "";
            $newCmdFlag = 0;
            ($prevPrevChar, $prevChar) = 0;
        } else {
            $cmd .= $char;
        }
        $prevPrevChar = $prevChar;
        $prevChar = $char;
        $quoteBalance = 0;
        foreach my $key (keys %quoteHash) {
            $quoteBalance += $quoteHash{$key};
        }
    }
    push(@cmds, $cmd."\n"); 

    # If conditional execution was detected in the above loop the appropriat flags are set
    my $condExec = $#cmds > 0 ? 1 : 0;
    my $condExecCount = 0;
    my $indentBackup = $indent;
    
    # For each line in the above decomposed statement, will be > 1 if conditional execution
    # was present in the line
    foreach my $cmdLine (@cmds) {
        chomp $cmdLine;
        my $condExecFlag = ($condExec and ($condExecCount != $#cmds)) ? chop $cmdLine : 0;
        next if $cmdLine =~ /^\s*$/;
        $cmdLine =~ s/^ *| *$//g;
        
        # Uses first word to obtain appropriate handling function
        $fTerm = $1 if $cmdLine =~ /([^ ]+)/;
        $fTerm = $typeFlag if (not defined $cmdToFunc{$fTerm} and $typeFlag);
        
        if ($line =~ /^#!/) {
            push (@outFile, "#!/usr/bin/python2.7 -u\n");
        } else {
            if (defined $cmdToFunc{$fTerm} ) {
                if ($cmdLine =~ /^\s*[^\\]#.*/) {
                    push (@outFile, $cmdLine."\n");
                } else {
                    # If command is present on line, seds comments handles the line then
                    # handles the line and adds the comment at the end
                    my $comment = $1  if ($cmdLine =~ s/[^\\\$](\#.*)//);
                    if (not $comment) {
                        (&{ $cmdToFunc{$fTerm} }($cmdLine, \@outFile, $index, \$indent)) 
                    } else {
                        my $newLine = pop(@outFile);
                        chomp $newLine;
                        $newLine .= $comment."\n";
                        push(@outFile, $newLine); 
                    }
                }
            } else {
                # If command is unknown checks whether its a variable assigning else ignores
                if ($cmdLine =~ /^([^ \$]+)=(.*)/) {
                    defVar($1, $2, \@outFile, $index, \$indent);
                }
            }
        }
        $condExecCount++;
        $index++;
        
        # Handles conditional execution where execution of statement
        # is dependent on previous command
        if ($condExecFlag eq "&") {
            my $convCmd = pop(@outFile);
            chomp $convCmd;
            push (@outFile, "if not ".$convCmd.":\n");
            incrIndent(0, 0, 0, \$indent);
        } elsif ($condExecFlag eq "|") {
            my $convCmd = pop(@outFile);
            chomp $convCmd;
            push (@outFile, "if ".$convCmd.":\n");
            incrIndent(0, 0, 0, \$indent);
        }
    }
    # Restores indentation prior to the conditional execution if flag is set
    $indent = $indentBackup if $condExec;           
}

close $tempFile;
$file =~ s/\..*$//;
writeToFile(\@outFile, $file.".py");
